var class_summary =
[
    [ "Summary", "class_summary.html#a17f9efd857f5cd9af6babb3f6d801f19", null ],
    [ "Summary", "class_summary.html#ae3e3709dc7d8ca7ff39a7c357c6ede57", null ],
    [ "GetMaxOutput", "class_summary.html#acfcb6bcb9b5999595c724d664c42e07f", null ],
    [ "GetMaxTime", "class_summary.html#a6a08dfac5203677cbf29b45daf634f7f", null ],
    [ "GetTotalOutput", "class_summary.html#a00286edc7af92ba4806939ec76e53cef", null ],
    [ "operator==", "class_summary.html#a79822652002c5fcb081d77810425e27c", null ],
    [ "SetMaxOutput", "class_summary.html#a2661c9f287d11dd8114e25ec44694c69", null ],
    [ "SetMaxTime", "class_summary.html#a62e6b7af901f6114b0215c8b2cd39013", null ],
    [ "SetTotalOutput", "class_summary.html#ae49fa334ba3965891c60cbbdf1ade4aa", null ]
];