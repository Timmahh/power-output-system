var class_power_records =
[
    [ "PowerRecords", "class_power_records.html#a0d86a9396ca66e3d37767c5e465cf077", null ],
    [ "AddRecord", "class_power_records.html#a45a209fcf89470b4937c8a7d42b669da", null ],
    [ "CreateSummaries", "class_power_records.html#a33b2114d8b8d8bf87b56570fb827818c", null ],
    [ "GetEqualTo", "class_power_records.html#a9400c0076762e4887beb80d45a7f0659", null ],
    [ "GetGreaterThan", "class_power_records.html#a201c16781b47171b7d0297c1c226c9c4", null ],
    [ "GetLessThan", "class_power_records.html#a0a63be479f6203e8ae2fd0993eac7d59", null ],
    [ "GetSummary", "class_power_records.html#a38c06070c491f0322e6a8b8011c0bca6", null ],
    [ "GetTimePeriods", "class_power_records.html#a847131bd8185acec31b5da7c2a5bbb0d", null ]
];