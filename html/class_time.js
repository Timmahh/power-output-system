var class_time =
[
    [ "Time", "class_time.html#a4245e409c7347d1d671858962c2ca3b5", null ],
    [ "Time", "class_time.html#a02621dc4ac3eae3904710d3fa1596c5f", null ],
    [ "getHour", "class_time.html#aabae3337c79b870d1bc1de05ea22dd17", null ],
    [ "getMeridiem", "class_time.html#a9d3e2c1bda9b76a2862f842c5a5a8f4b", null ],
    [ "getMinute", "class_time.html#a608d2d009acec5813efe85fd788bea25", null ],
    [ "operator==", "class_time.html#a703ed15b4968c721ce494492451c3192", null ],
    [ "setHour", "class_time.html#a6cbab631c3044612059af63c8ff9e62e", null ],
    [ "setMeridiem", "class_time.html#aab80ea35171fffdf1616fcef034c9639", null ],
    [ "setMinute", "class_time.html#aad64aadebef268e0d87517897a25c15b", null ],
    [ "setTime", "class_time.html#a6422ba71f240197dc08b87250847a7e9", null ]
];