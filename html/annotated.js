var annotated =
[
    [ "BinaryTree< T >", "class_binary_tree.html", "class_binary_tree" ],
    [ "Date", "class_date.html", "class_date" ],
    [ "FileInput", "class_file_input.html", "class_file_input" ],
    [ "FileOutput", "class_file_output.html", "class_file_output" ],
    [ "Menu", "class_menu.html", "class_menu" ],
    [ "MonthYear", "struct_month_year.html", "struct_month_year" ],
    [ "Node< T >", "struct_node.html", "struct_node" ],
    [ "PowerRecords", "class_power_records.html", "class_power_records" ],
    [ "Reading", "class_reading.html", "class_reading" ],
    [ "Summary", "class_summary.html", "class_summary" ],
    [ "Time", "class_time.html", "class_time" ]
];