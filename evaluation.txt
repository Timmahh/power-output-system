---------
Positives
---------
	-> I really improved on my first assignment turning some of the
	negatives I identified as well as using the assignment feedback to
	improve the design of my program. Some changes include the isValidDate
	function in the date class and using the equation:
		kWh = kW * (1 / 12)
	to work out the power output for each record.

	-> Convenience - I really like it how this program can be used to
	generate the file list as well as being able to use it for any number of
	months and years.

---------
Negatives
---------
	-> The menu requires you to type '4' twice before it will quit but only
	if you have used one of the other options first. For some silly reason
	unknown to me it did this but I was sick of fiddling with that class.
	
	-> Time management - my time management for this assignment was poor
	partially due to large assignments for ICT306 and ICT215 although I
	feel like I should have made better use of my time.

	-> Source control - the one thing that really REALLY dropped off from
	the first assignment. I really have to start learning GIT.

	-> Wasn't actually tested on a UNIX system.I deleted my UNIX partitions
	just before I realised I would need them because of poor support for
	my video card in Ubuntu 12.04.

-----------
Assumptions
-----------
	-> That the data files are in the "data" directory which is in the same
	file as the executable.