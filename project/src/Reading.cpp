#include "Reading.h"

//-----------------------------------------------------------------------------

Reading::Reading()
{
    timeOfDay = Time();
    powerOutput = 0.0;
}

//-----------------------------------------------------------------------------

Reading::Reading(const Time &tm, const double out)
{
    timeOfDay = tm;
    SetPowerOutput(out);
}

//-----------------------------------------------------------------------------

Time Reading::GetTime() const
{
    return timeOfDay;
}

//-----------------------------------------------------------------------------

void Reading::SetTime(const Time &tm)
{
    timeOfDay = tm;
}

//-----------------------------------------------------------------------------

double Reading::GetPowerOutput() const
{
    return powerOutput;
}

//-----------------------------------------------------------------------------

bool Reading::SetPowerOutput(const double out)
{
    if(out >= 0.0)
    {
        powerOutput = out;
        return true;
    }
    else
    {
        return false;
    }
}

//-----------------------------------------------------------------------------

bool Reading::operator <=(const Reading &rhs) const
{
	return (powerOutput <= rhs.GetPowerOutput());
}

//-----------------------------------------------------------------------------

Reading& Reading::operator =(const Reading &other)
{
	if(this != &other)
	{
		SetTime(other.GetTime());
		powerOutput = other.powerOutput;
	}

	return *this;
}

//-----------------------------------------------------------------------------
