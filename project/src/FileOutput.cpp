#include "FileOutput.h"

//-----------------------------------------------------------------------------

bool FileOutput::outputMonth(const MonthYear monthAndYear)
{
	char fileName[50];
	ofstream outFile;
	Date currentDate;

	currentDate.SetMonth(monthAndYear.month);
	currentDate.SetYear(monthAndYear.year);

	string monthPrefix = currentDate.GetMonthPrefix();

	sprintf(fileName, "%s%u.txt", monthPrefix.c_str(), monthAndYear.year);

	outFile.open(fileName);

	if(!outFile.is_open())
	{
		return false;
	}

	for(unsigned i = 1; i <= currentDate.GetNoDays(); i++)
	{
		currentDate.SetDayOfMonth(i);
		outFile.width(2);
		outFile.fill('0');
		outFile << currentDate.GetDayOfMonth() << "/" << currentDate.GetMonth()
			<< "/" << currentDate.GetYear() << ";";

		Summary temp = PowerRecords::GetSummary(currentDate);


		if(temp == Summary())
		{
			// there are no records for the given date
			outFile << endl;
		}
		else
		{
			outFile.width(5);
			outFile << temp.GetTotalOutput() << ";" <<
						temp.GetMaxOutput() << ";";

			outFile.width(2);
			if(temp.GetMaxTime().getMeridiem() == 0)
			{
				outFile << temp.GetMaxTime().getHour() << ":";
				outFile << setw(2) << temp.GetMaxTime().getMinute()
					<< " AM" << endl;
			}
			else
			{
				outFile << temp.GetMaxTime().getHour() << ":";
				outFile << setw(2) << temp.GetMaxTime().getMinute()
					<< " PM" << endl;
			}
		}
	}

	outFile << "Total Number of Days: " << currentDate.GetNoDays();

	outFile.close();

	return true;
}

//-----------------------------------------------------------------------------
