#include "BinaryTree.h"
#include <iostream>

using namespace std;

void test01(); // Constructor test
void test02(); // First insert test
void test03(); // Left insert tree test
void test04(); // Right insert tree test
void test05(); // Removing element that does not exist test
void test06(); // Removing root test
void test07(); // Removing left element test
void test08(); // Removing right element test
void test09(); // In order traversal test
void test10(); // Pre order traversal test
void test11(); // Post order traversal test
void test12(); // Copy constructor test
void test13(); // Assignment operator test

int main()
{
    BinaryTree<int> tree;

	test01();
	test02();
	test03();
	test04();
	test05();
	test06();
	test07();
	test08();
	test09();
	test10();
	test11();
	test12();
	test13();
}

void print(int &x)
{
	cout << x << endl;
}

void test01() // Constructor test
{
	cout << "Test 01 - Constructor test" << endl;

	BinaryTree<int> test01;

	test01.inOrderTraversal(print);
	cout << endl;
}

void test02() // First insert test
{
	cout << "Test 02 - First insert test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test03() // Left insert tree test
{
	cout << "Test 03 - Left insert tree test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(2);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test04() // Right insert tree test
{
	cout << "Test 04 - Right insert tree test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test05() // Removing element that does not exist test
{
	cout << "Test 05 - Removing element that does not exist test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(1);
	test01.remove(2);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test06() // Removing root test
{
	cout << "Test 06 - Removing root test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(4);
	test01.remove(3);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test07() // Removing left element test
{
	cout << "Test 07 - Removing left element test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(1);
	test01.remove(1);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test08() // Removing right element test
{
	cout << "Test 08 - Removing right element test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(1);
	test01.remove(7);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test09() // In order traversal test
{
	cout << "Test 09 - In order traversal test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(6);
	test01.insert(4);
	test01.insert(9);
	test01.insert(1);

	test01.inOrderTraversal(print);
	cout << endl;
}

void test10() // Pre order traversal test
{
	cout << "Test 10 - Pre order traversal test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(6);
	test01.insert(4);
	test01.insert(9);
	test01.insert(1);

	test01.preOrderTraversal(print);
	cout << endl;
}

void test11() // Post order traversal test
{
	cout << "Test 11 - Post order traversal test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(6);
	test01.insert(4);
	test01.insert(9);
	test01.insert(1);

	test01.postOrderTraversal(print);
	cout << endl;
}

void test12() // Copy constructor test
{
	cout << "Test 12 - Copy constructor test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(6);
	test01.insert(4);
	test01.insert(9);
	test01.insert(1);

	BinaryTree<int> test02 = BinaryTree<int>(test01);

	test02.inOrderTraversal(print);
	cout << endl;
}

void test13() // Assignment operator test
{
	cout << "Test 13 - Assignment operator test" << endl;

	BinaryTree<int> test01;
	test01.insert(3);
	test01.insert(7);
	test01.insert(6);
	test01.insert(4);
	test01.insert(9);
	test01.insert(1);

	BinaryTree<int> test02 = test01;

	test02.inOrderTraversal(print);
	cout << endl;
}
