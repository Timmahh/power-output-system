#include "Menu.h"

//-----------------------------------------------------------------------------

void Menu::CreateMenu()
{
	char inputValue;

	do
	{
	    displayMenu();
	    inputValue = getInput();

		switch(inputValue)
		{
			case '1':
				lessThan();
				CreateMenu();
				break;
			case '2':
				greaterThan();
				CreateMenu();
				break;
			case '3':
				equalTo();
				CreateMenu();
				break;
			case '4':
				break;
			default:
                cout << "Invalid Selection" << endl;
                CreateMenu();
                break;
		}
	} while(inputValue != '4');

}

//-----------------------------------------------------------------------------

void Menu::displayMenu()
{
	cout << "Select one of the following: " << endl;
	cout << "1. Display dates on which the electricity production was " << endl;
	cout << "   BELOW the specified number of units." << endl;
	cout << "2. Display dates on which the electricity production was " << endl;
	cout << "   ABOVE the specified number of units." << endl;
	cout << "3. Display dates on which the electricity production was " << endl;
	cout << "   EQUAL to the specified number of units." << endl;
	cout << "4. To EXIT" << endl;
}

//-----------------------------------------------------------------------------

char Menu::getInput()
{
	char inputValue;

	cin >> inputValue;
	cin.ignore(100, '\n');
	cin.clear();

	return inputValue;
}

//-----------------------------------------------------------------------------

void Menu::lessThan()
{
	string input;

	do {
		cout << "Enter a value: " << endl;
		cin >> input;
	} while(!checkIfDouble(input));

	displayOutput(PowerRecords::GetLessThan(atof(input.c_str())));
}

//-----------------------------------------------------------------------------

void Menu::greaterThan()
{
	string input;

	do {
		cout << "Enter a value: " << endl;
		cin >> input;
	}
	while(!checkIfDouble(input));

	displayOutput(PowerRecords::GetGreaterThan(atof(input.c_str())));
}

//-----------------------------------------------------------------------------

void Menu::equalTo()
{
	string input;

	do {
		cout << "Enter a value: " << endl;
		cin >> input;
	} while(!checkIfDouble(input));

	displayOutput(PowerRecords::GetEqualTo(atof(input.c_str())));
}

//-----------------------------------------------------------------------------

void Menu::displayOutput(vector<Date> output)
{
	int counter = 0;
	string enter;
	bool display = false;

	for(unsigned i = 0; i < output.size(); i++)
	{
		displayDate(output.at(i));
		counter++;

		if(counter > 5)
		{
			counter = 0;
			if(display)
			{
				cout << "-- Press ENTER to view more --";
			}
			cin.get();
			display = true;
		}
	}
}

//-----------------------------------------------------------------------------

void Menu::displayDate(Date &display)
{
	cout << display.GetDayOfMonth() << "/" << display.GetMonth() << "/" <<
			display.GetYear() << endl;
}

//-----------------------------------------------------------------------------

bool Menu::checkIfDouble(string val)
{
	bool isValid = true;

	for(unsigned i = 0; i < val.length(); i++)
	{
		if(!(isdigit(val[i]) || val.at(i) == '.'))
		{
			isValid = false;
		}
	}

	return isValid;
}

//-----------------------------------------------------------------------------
