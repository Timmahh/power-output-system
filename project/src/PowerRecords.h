#ifndef POWERRECORDS_H
#define POWERRECORDS_H

//-----------------------------------------------------------------------------

#include <map>
#include <iostream>
#include <vector>
#include <stdexcept>
#include "Date.h"
#include "Reading.h"
#include "Summary.h"
#include "BinaryTree.h"

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------

typedef map<Date, BinaryTree<Reading> > RecordsAtDate;
typedef RecordsAtDate::iterator RecItr;
typedef RecordsAtDate::const_iterator RecCItr;

//-----------------------------------------------------------------------------

typedef map<Date, Summary> SummaryAtDate;
typedef SummaryAtDate::iterator SumItr;
typedef SummaryAtDate::const_iterator SumCItr;

//-----------------------------------------------------------------------------

struct MonthYear
{
	unsigned month;
	unsigned year;
};

//-----------------------------------------------------------------------------

typedef vector<MonthYear> monthYearVector;
typedef monthYearVector::iterator monthItr;

//-----------------------------------------------------------------------------
/**
 * \file PowerRecords.h PowerRecords.cpp
 * \class PowerRecords
 * \brief Power Records class implementation.
 *
 * Collates all the records from the files into a map and then processes them
 * to form summary objects which form the file output. Also provides functions
 * for identifying days where the total output is is less than, greater than
 * or equal to a specified value.
 *
 * \author Timothy Veletta
 * \date 26/05/12
 * \version 01 - Timothy Veletta, 26/05/12
 *          Created the constructor, addRecord, createSummaries and GetSummary
 *			functions as well as the related member variables.
 * \version 02 - Timothy Veletta, 27/05/12
 *
 */
class PowerRecords
{
    public:
		/**
		 * \brief The default constructor.
		 *
		 * The default constructor for the PowerRecords class simply sets the
		 * currentTotal and currentMaxOutput to 0 and calls the default
		 * constructor for currentMaxTime.
		 */
        PowerRecords();

		/**
		 * \brief Adds a record.
		 *
		 * Adds a record which included the time and power output on a
		 * particular day.
		 *
		 * \param dateOfRecord the date of the record
		 * \param timeOfRecord the time of the record
		 * \param powerOutput the power output at that time
		 * \retval whether or not the operation was successful
		 */
        static bool AddRecord(const Date &dateOfRecord,
							const Time &timeOfRecord,
							const double powerOutput);

		/**
		 * \brief Creates the summary objects.
		 *
		 * Creates the summary objects from the records that have been read
		 * into the class. A summary contains the total output for the day,
		 * max output and time of the max output.
		 */
		void CreateSummaries();

		/**
		 * \brief Gets the time periods.
		 *
		 * Gets the time periods vector which is a vector containing
		 * what months and years have been input into the system. Each month
		 * and year pairing can only be mentioned once.
		 *
		 * \retval what months and years have been input
		 */
		vector<MonthYear> GetTimePeriods();

		/**
		 * \brief Gets the summary object.
		 *
		 * Gets the summary object for the given date.
		 *
		 * \param summaryDate the date of the summary
		 * \retval the summary
		 */
		static Summary& GetSummary(const Date &summaryDate);


		/**
		 * \brief Returns dates where the output is less than specified.
		 *
		 * Returns a vector containing all the dates in which the total output
		 * is less than the the given value.
		 *
		 * \param outputValue the comparison output value
		 * \retval the vector of dates
		 */
		static vector<Date> GetLessThan(const double outputValue);

		/**
		 * \brief Returns dates where the output is more than specified.
		 *
		 * Returns a vector containing all the dates in which the total output
		 * is greater than the given value.
		 *
		 * \param outputValue the comparison output value
		 * \retval the vector of dates
		 */
		static vector<Date> GetGreaterThan(const double outputValue);

		/**
		 * \brief Returns dates where the output is equal to specified.
		 *
		 * Returns a vector containing all the dates in which the total output
		 * is equal to the given value (within a tolerance of +- 0.001).
		 *
		 * \param outputValue the comparison output value
		 * \retval the vector of dates
		 */
		static vector<Date> GetEqualTo(const double outputValue);
    protected:
    private:
		static RecordsAtDate records; //!< all the input records at date
		static SummaryAtDate summaries; //!< all the summary objects at date

		static double currentTotal; /*!< the current total for use in the
										summary objects */
		static double currentMaxOutput; /*!< the current max output for use in
										the summary objects */
		static Time currentMaxTime; /*!< the current max time for use in the
										summary objects */

		static void maximum(Reading &val);
		static void total(Reading &val);
};

//-----------------------------------------------------------------------------

#endif // POWERRECORDS_H
