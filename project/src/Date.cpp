#include "Date.h"

//-----------------------------------------------------------------------------

Date::Date()
{
    dayOfMonth = 1;
    month = 1;
    year = 1970;
}

//-----------------------------------------------------------------------------

Date::Date(const int dd, const int mm, const int yyyy)
{
	SetYear(yyyy);
	SetMonth(mm);
	SetDayOfMonth(dd);
}

//-----------------------------------------------------------------------------

int Date::GetYear() const
{
	return year;
}

//-----------------------------------------------------------------------------

bool Date::SetYear(const int val)
{
	if(isValidDate(dayOfMonth, month, val))
	{
		year = val;
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

int Date::GetMonth() const
{
	return month;
}

//-----------------------------------------------------------------------------

bool Date::SetMonth(const int val)
{
	if(isValidDate(dayOfMonth, val, year))
	{
		month = val;
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

int Date::GetDayOfMonth() const
{
	return dayOfMonth;
}

//-----------------------------------------------------------------------------

bool Date::SetDayOfMonth(const int val)
{
	if(isValidDate(val, month, year))
	{
		dayOfMonth = val;
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

bool Date::isLeapYear(const int val) const
{
	if((val % 4 == 0 && val % 100 != 0) || val % 400 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

string Date::GetMonthPrefix() const
{
	string prefix[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug"
							,"Sep", "Oct", "Nov", "Dec"};

	return prefix[month - 1];
}

//-----------------------------------------------------------------------------

unsigned Date::GetNoDays() const
{
    return daysInMonth[month - 1];
}

//-----------------------------------------------------------------------------

bool Date::isValidDate(const int dd, const int mm, const int yyyy) const
{
	bool valid = true;

	if(yyyy < 0) {
		valid = false;
	}

	if(mm > 12 || mm < 1) {
		valid = false;
	}

	if(!((dd > 0 && dd <= daysInMonth[mm - 1]) ||
		((mm == 2 && isLeapYear(yyyy)) && dd == 29))) {
		valid = false;
	}

	return valid;
}

//-----------------------------------------------------------------------------

bool Date::operator <(const Date &rhs) const
{
	if(year < rhs.year)
	{
		return true;
	}
	else if((year == rhs.year) && month < rhs.month)
	{
		return true;
	}
	else if((month == rhs.month) && dayOfMonth < rhs.dayOfMonth)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------
