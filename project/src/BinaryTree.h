#ifndef BINARYTREE_H
#define BINARYTREE_H

//-----------------------------------------------------------------------------

#include "Node.h"
#include <iostream>

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------
/**
 * \file BinaryTree.h
 * \class BinaryTree
 * \brief FileInput class implementation.
 *
 * Creates a binary tree and allows the user to add elements, delete elements,
 * and traverse the tree.
 *
 * \author Timothy Veletta
 * \date 07/05/2012
 * \version 01 - Timothy Veletta, 07/05/12
 *          Created the default constructor, second constructor, setFileList,
 *          getFileList, processFiles and processLine functions and their
 *          implementations.
 * \version 02 - Timothy Veletta, 26/05/12
 *          Removed both constructors and the public setFileList function.
 *          Added the directory member variable and the setVariable function.
 */
template <class T>
class BinaryTree
{
    public:
		/**
		 * \brief BinaryTree constructor.
		 *
		 * The constructor for the BinaryTree class initialises the root to
		 * NULL.
		 */
        BinaryTree();

		/**
		 * \brief Copy constructor for the BinaryTree.
		 *
		 * Copy constructor for the BinaryTree allows an existing Binary Tree
		 * be copied to a new Binary Tree.
		 *
		 * \param otherTree the tree to copy from
		 */
		BinaryTree(const BinaryTree<T> &otherTree);

		/**
		 * \brief The destructor for the BinaryTree.
		 *
		 * The destructor for the Binary Tree destroys the tree.
		 */
		~BinaryTree();

		/**
		 * \brief The assignment operator.
		 *
		 * The assignment operator for the Binary Tree copies the other Binary
		 * tree.
		 *
		 * \param otherTree the tree to copy from
		 * \retval a reference to this
		 */
        BinaryTree<T>& operator =(const BinaryTree<T> &otherTree) const;

		/**
		 * \brief Returns if the tree is empty.
		 *
		 * Returns whether or not the tree is empty. It is considered empty if
		 * the root is NULL.
		 *
		 * \retval whether or not the tree is empty
		 */
        bool isEmpty() const;

		/**
		 * \brief Inserts an element into the tree.
		 *
		 * Inserts the given element into the tree. If the tree is empty, then
		 * that element is inserted at the root. Otherwise it is inserted at
		 * the appropriate place in the tree.
		 *
		 * \param data the data to be inserted into the tree
		 * \retval whether or not the operation was successful
		 */
        bool insert(const T& data);

		/**
		 * \brief Removes an element from the tree.
		 *
		 * Removes the given element from the tree and returns whether or not
		 * the operation was successful.
		 *
		 * \param element the element to remove from the tree
		 * \retval whether or not the operation was successful
		 */
        bool remove(const T& element);

		/**
		 * \brief Traverses the binary tree.
		 *
		 * Traverses the binary tree using the in order method and performs
		 * the input operation upon visiting each element.
		 *
		 * \param func the operation to perform upon visiting each element
		 */
        void inOrderTraversal(void (*func) (T &val)) const;

        /**
		 * \brief Traverses the binary tree.
		 *
		 * Traverses the binary tree using the pre order method and performs
		 * the input operation upon visiting each element.
		 *
		 * \param func the operation to perform upon visiting each element
		 */
        void preOrderTraversal(void (*func) (T &val)) const;

        /**
		 * \brief Traverses the binary tree.
		 *
		 * Traverses the binary tree using the post order method and performs
		 * the input operation upon visiting each element.
		 *
		 * \param func the operation to perform upon visiting each element
		 */
        void postOrderTraversal(void (*func) (T &val)) const;
    protected:
		Node<T> *root; //!< the root of the tree
    private:
        void inOrder(Node<T> *nd, void (*func) (T &val)) const;
        void preOrder(Node<T> *nd, void (*func) (T &val)) const;
        void postOrder(Node<T> *nd, void (*func) (T &val)) const;

        void deleteFromTree(Node<T>* &p);

        void copyTree(Node<T>* &copiedTreeRoot, Node<T>* otherTreeRoot);

        void destroy(Node<T>* &nd);
};

//-----------------------------------------------------------------------------

template <class T>
BinaryTree<T>::BinaryTree()
{
    root = NULL;
}

//-----------------------------------------------------------------------------

template <class T>
BinaryTree<T>::BinaryTree(const BinaryTree<T> &otherTree)
{
	if(otherTree.root == NULL)
	{
		root = NULL;
	}
	else
	{
		copyTree(root, otherTree.root);
	}
}

//-----------------------------------------------------------------------------

template <class T>
BinaryTree<T>::~BinaryTree<T>()
{
	destroy(root);
}

//-----------------------------------------------------------------------------

template <class T>
BinaryTree<T>& BinaryTree<T>::operator =(const BinaryTree<T> &otherTree) const
{
	if(this != &otherTree)
	{
		if(root != NULL)
		{
			destroy(root);
		}

		if(otherTree.root == NULL)
		{
			root = NULL;
		}
		else
		{
			copyTree(root, otherTree.root);
		}
	}

	return *this;
}

//-----------------------------------------------------------------------------

template <class T>
bool BinaryTree<T>::isEmpty() const
{
	return (root == NULL);
}

//-----------------------------------------------------------------------------

template <class T>
bool BinaryTree<T>::insert(const T& data)
{
    Node<T> *newNode = new Node<T>(data);
    Node<T> *currentNode = this->root;

	// if there is no root, insert the current data into the root
    if (this->root == NULL) {
        this->root = newNode;
        return true;
    }

    while(currentNode != NULL)
    {
    	// if the data already exists then insert it to the left of the
    	// current element
		if (data <= currentNode->data)
        {
            if(currentNode->left == NULL)
            {
            	// insert into left
                currentNode->left = newNode;
                return true;
            }
            else
            {
                currentNode = currentNode->left;
            }
        }
        else
        {
            if(currentNode->right == NULL)
            {
            	// insert into right
                currentNode->right = newNode;
                return true;
            }
            else
            {
                currentNode = currentNode->right;
            }
        }
    }

    delete newNode;
    delete currentNode;

    return true;
}

//-----------------------------------------------------------------------------

template <class T>
bool BinaryTree<T>::remove(const T& element)
{
	Node<T> *current;
	Node<T> *previous;
	bool found = false;

	if(this->root == NULL)
	{
		// there are no element in the tree
		return false;
	}
	else
	{
		current = root;
		previous = root;

		while(current != NULL && !found)
		{
			if(current->data == element)
			{
				found = true;
			}
			else
			{
				previous = current;

				if(current->data > element)
				{
					current = current->left;
				}
				else
				{
					current = current->right;
				}
			}
		}

		if(current == NULL)
		{
			// element not found
			return false;
		}
		else if(found)
		{
			if(current == root) {
				deleteFromTree(root);
				return true;
			}
			else if(previous-> data > element)
			{
				deleteFromTree(previous->left);
				return true;
			}
			else
			{
				deleteFromTree(previous->right);
				return true;
			}
		}
		else
		{
			// element not found
			return false;
		}
	}
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::deleteFromTree(Node<T>* &p)
{
	Node<T> *current;
	Node<T> *previous;
	Node<T> *temp;

	if(p == NULL)
	{
		// node to be deleted is null
	}
	else if(p->left == NULL && p->right == NULL)
	{
		temp = p;
		p = NULL;
		delete temp;
	}
	else if(p->left == NULL)
	{
		temp = p;
		p = temp->right;
		delete temp;
	}
	else if(p->right == NULL)
	{
		temp = p;
		p = temp->left;
		delete temp;
	}
	else
	{
		current = p->left;
		previous = NULL;

		while(current->right != NULL)
		{

		}
	}
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::inOrder(Node<T> *nd, void (*func) (T &val)) const
{
    if(nd != NULL)
    {
        inOrder(nd->left, func);
        (*func)(nd->data);
        inOrder(nd->right, func);
    }
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::inOrderTraversal(void (*func) (T &val)) const
{
    inOrder(root, func);
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::preOrder(Node<T> *nd, void (*func) (T &val)) const
{
	if(nd != NULL)
	{
		(*func)(nd->data);
		preOrder(nd->left, func);
		preOrder(nd->right, func);
	}
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::preOrderTraversal(void (*func) (T &val)) const
{
	preOrder(root, func);
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::postOrder(Node<T> *nd, void (*func) (T &val)) const
{
	if(nd != NULL)
	{
		postOrder(nd->left, func);
		postOrder(nd->right, func);
		(*func)(nd->data);
	}
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::postOrderTraversal(void (*func) (T &val)) const
{
	postOrder(root, func);
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::copyTree(Node<T>* &copiedTreeRoot, Node<T>* otherTreeRoot)
{
	if(otherTreeRoot == NULL)
	{
		copiedTreeRoot = NULL;
	}
	else
	{
		copiedTreeRoot = new Node<T>;
		copiedTreeRoot->data = otherTreeRoot->data;
		copyTree(copiedTreeRoot->left, otherTreeRoot->left);
		copyTree(copiedTreeRoot->right, otherTreeRoot->right);
	}
}

//-----------------------------------------------------------------------------

template <class T>
void BinaryTree<T>::destroy(Node<T>* &nd)
{
	if(nd != NULL)
	{
		destroy(nd->left);
		destroy(nd->right);
		delete nd;
		nd = NULL;
	}
}

//-----------------------------------------------------------------------------

#endif // BINARYTREE_H
