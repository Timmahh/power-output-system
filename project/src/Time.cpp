#include "Time.h"

//-----------------------------------------------------------------------------

Time::Time()
{
	hour = 12;
	minute = 0;
	ampm = AM;
}

//-----------------------------------------------------------------------------

Time::Time(const unsigned hh, const unsigned mm, const Meridiem ap)
{
	setHour(hh);
	setMinute(mm);
	setMeridiem(ap);
}

//-----------------------------------------------------------------------------

unsigned Time::getHour() const
{
	return hour;
}

//-----------------------------------------------------------------------------

bool Time::setHour(const unsigned hh)
{
	if(hh >= 1 && hh <= 12)
	{
		hour = hh;
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

unsigned Time::getMinute() const
{
	return minute;
}

//-----------------------------------------------------------------------------

bool Time::setMinute(const unsigned mm)
{
	if(mm >= 0 && mm<= 59)
	{
		minute = mm;
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

Meridiem Time::getMeridiem() const
{
	return ampm;
}

//-----------------------------------------------------------------------------

bool Time::setMeridiem(const Meridiem ap)
{
	if(ap == AM || ap == PM)
	{
		ampm = ap;
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

bool Time::setTime(const unsigned hh, const unsigned mm, const Meridiem ap)
{
	if(setHour(hh) && setMinute(mm) && setMeridiem(ap))
	{
		return true;
	}
	else
	{
		return false;
	}
}

//-----------------------------------------------------------------------------

bool Time::operator ==(const Time &rhs) const
{
	return ((hour == rhs.hour) &&
			(minute == rhs.minute) &&
			(ampm == rhs.ampm));
}

//-----------------------------------------------------------------------------
