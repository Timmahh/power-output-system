#ifndef NODE_H
#define NODE_H

//-----------------------------------------------------------------------------

#include <cstddef>

//-----------------------------------------------------------------------------
/**
 * \file Node.h
 * \class Node
 * \brief Node structure implementation.
 *
 * Holds a data member, left node and right node for use in a binary tree.
 *
 * \author Timothy Veletta
 * \date 27/05/12
 * \version 01 - Timothy Veletta, 27/05/12
 *          Created the data, left and right memeber variables as well as the
 *          3 constructors.
 */
template <class T>
struct Node
{
    T data; //!< the data to be held by the node
    Node<T> *left; //!< the node to the left
    Node<T> *right; //!< the node to the right

    /**
     * \brief Sets up the node.
     *
     * Sets up the node by initialising the member variables of the struct.
     */
    Node();

    /**
     * \brief Sets up the node with a data member.
     *
     * Sets up the node by initialising the member variables and setting the
     * data member.
     *
     * \param dt the data member
     */
    Node(T dt);

    /**
     * \brief Sets up the node with a data member, left node and right node.
     *
     * Sets up the node by initialising the member variables and setting the
     * data member, left node and right node.
     *
     * \param dt the data member
     * \param lft the left node
     * \param rgh the right node
     */
    Node(T dt, Node<T> *lft, Node<T> *rgh);
};

//-----------------------------------------------------------------------------

template <class T>
Node<T>::Node()
{
    data = T();
    left = NULL;
    right = NULL;
}

//-----------------------------------------------------------------------------

template <class T>
Node<T>::Node(T dt)
{
    data = dt;
    left = NULL;
    right = NULL;
}

//-----------------------------------------------------------------------------

template <class T>
Node<T>::Node(T dt, Node<T> *lf, Node<T> *rgh)
{
    data = dt;
    left = lf;
    right = rgh;
}

//-----------------------------------------------------------------------------

#endif
