#ifndef SUMMARY_H
#define SUMMARY_H

//-----------------------------------------------------------------------------

#include "Time.h"

//-----------------------------------------------------------------------------

/**
 * \file Summary.h Summary.cpp
 * \class Summary
 * \brief Summary class implementation.
 *
 * Holds a summary of a days worth of power output readings including the total
 * output for the day, the maximum output and the time that the maximum output
 * occured.
 *
 * \author Timothy Veletta
 * \date 27/05/12
 * \version 01 - Timothy Veletta, 27/05/12
 *          Created the default constructor, second constructor and the get and
 *          set functions for the member variables, totalOutput, maxOutput and
 *          maxTime.
 * \version 02 - Timothy Veletta, 28/05/12
 *          Added the equals operator.
 */
class Summary
{
    public:
        /**
         * \brief The default constructor.
         *
         * The default constructor sets the output and maximum output to 0 and
         * calls the default constructor for the max time.
         */
        Summary();

        /**
         * \brief The second constructor.
         *
         * Sets the total output for the day, the maximum output and the time
         * that the maximum output occurs.
         *
         * \param out the total output
         * \param maxOut the maximum output
         * \param maxTime the time of the maximum output
         */
        Summary(const double out, const double maxOut, const Time &maxTime);

        /**
         * \brief Gets the total output.
         *
         * Returns the total power output for the given set of power records.
         *
         * \retval the total output
         */
        double GetTotalOutput() const;

        /**
         * \brief Sets the total output.
         *
         * Sets the total power output to the given input value. This value
         * cannot be less than 0.
         *
         * \param out the total power output
         * \retval whether or not the operation was successful
         */
        bool SetTotalOutput(const double out);

        /**
         * \brief Gets the maximum power output.
         *
         * Returns the maximum power output for the given set of power records.
         *
         * \retval the maximum power output
         */
        double GetMaxOutput() const;

        /**
         * \brief Sets the maximum power output.
         *
         * Sets the maximum power output to the given input value. This value
         * cannot be less than 0.
         *
         * \param maxOut the maximum power output
         * \retval whether or not this operation was successful
         */
        bool SetMaxOutput(const double maxOut);

        /**
         * \brief Gets the time of the maximum power output.
         *
         * Returns the time of the maximum power output for the given set of
         * power records.
         *
         * \retval the time of the maximum power output
         */
        Time& GetMaxTime();

        /**
         * \brief Sets the time of the maximum power output.
         *
         * Sets the time of the maximum power output to the given input value.
         *
         * \param maxTime the time of the maximum power output
         */
        void SetMaxTime(const Time &maxTime);

        /**
         * \brief The equality operator.
         *
         * Returns whether or not the left hand side is equal to the right hand
         * side. Two Summary objects are said to be equal if the totalOutput,
         * maxOutput and timeOfMaximum are equal.
         *
         * \param rhs the Summary to be compared
         * \retval whether or not the two objects are equal
         */
        bool operator==(const Summary &rhs) const;
    protected:
    private:
        double totalOutput; //!< the total power output
        double maxOutput; //!< the maximum power output
        Time timeOfMaximum; //!< the time of the maximum power output
};

//-----------------------------------------------------------------------------

#endif // SUMMARY_H
