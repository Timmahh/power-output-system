#ifndef FILEINPUT_H
#define FILEINPUT_H

//-----------------------------------------------------------------------------

#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>

#include "Reading.h"
#include "Date.h"
#include "Time.h"
#include "PowerRecords.h"

//-----------------------------------------------------------------------------

#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------

/**
 * \file FileInput.h FileInput.cpp
 * \class FileInput
 * \brief FileInput class implementation.
 *
 * Gets the file list provided, then opens each file and processes them
 * creating Power objects.
 *
 * \author Timothy Veletta
 * \date 07/05/2012
 * \version 01 - Timothy Veletta, 07/05/12
 *          Created the default constructor, second constructor, setFileList,
 *          getFileList, processFiles and processLine functions and their
 *          implementations.
 * \version 02 - Timothy Veletta, 26/05/12
 *          Removed both constructors and the public setFileList function.
 *          Added the directory member variable and the setVariable function.
 */
class FileInput
{
	public:
		/**
		 * \brief Sets the output directory.
		 *
		 * Sets the output directory and then reads and processes the files
		 * in that directory.
		 *
		 * \param dir the directory to read from
		 * \retval whether or not the operation is successful
		 */
        static bool setDirectory(const string dir);
	protected:
	private:
        static string directory; //!< the directory to read from
		static string file; //!< the filelist file
		static vector<string> fileList; /*!< contains the name of the records
												files */

        static bool createFileList();
		static bool getFileList();
		static bool processFiles();
		static bool processLine(string line);
};

//-----------------------------------------------------------------------------

#endif // FILEINPUT_H
