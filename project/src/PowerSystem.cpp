#include "FileInput.h"
#include "FileOutput.h"
#include "Menu.h"

//-----------------------------------------------------------------------------

int main()
{
	FileInput::setDirectory("./data/");

	PowerRecords records = PowerRecords();

	records.CreateSummaries();

	monthYearVector months = records.GetTimePeriods();

	for(unsigned i = 0; i < months.size(); i++)
	{
		FileOutput::outputMonth(months.at(i));
	}

	Menu::CreateMenu();

	return 0;
}

//-----------------------------------------------------------------------------
