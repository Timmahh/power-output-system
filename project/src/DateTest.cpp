// DateTest.cpp

//------------------------------------------------------------------------------

#include "Date.h"

//------------------------------------------------------------------------------

void Test1(); // Constructor Test
void Test2(); // Year setting Test
void Test3(); // Negative year Test
void Test4(); // Month setting Test
void Test5(); // Negative month Test
void Test6(); // Month greater than 12 Test
void Test7(); // Month = 0 Test
void Test8(); // Day of month setting Test
void Test9(); // Negative day of month Test
void Test10(); // 31 days in month fail Test
void Test11(); // 29 days Feb non-leap year Test
void Test12(); // 29 days Feb leap year Test
void Test13(); // Day of month = 0 Test
void Test14(); // Month prefix Test
void Test15(); // Number of days Test

//-----------------------------------------------------------------------------

int main()
{
    Date date;

    cout << "Date Test Program" << endl << endl;

    Test1();
    Test2();
    Test3();
    Test4();
    Test5();
    Test6();
    Test7();
    Test8();
    Test9();
    Test10();
    Test11();
    Test12();
    Test13();
    Test14();
    Test15();

    cout << endl;
    return 0;
}

//-----------------------------------------------------------------------------

void Test1() // Constructor Test
{
    Date date;

    cout << "Test 1 - Constructor test" << endl;
    cout << "year = " << date.GetYear() << endl <<
            "month = " << date.GetMonth() << endl <<
            "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test2()  // Year setting Test
{
    Date date;

    cout << "Test 2 - Year setting test" << endl;
    date.SetYear(2012);
    cout << "year = " << date.GetYear() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test3() // Negative year Test
{
    Date date;

    cout << "Test 3 - Negative year test" << endl;
    date.SetYear(-1);
    cout << "year = " << date.GetYear() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test4() // Month setting Test
{
    Date date;

    cout << "Test 4 - Month setting test" << endl;
    date.SetMonth(5);
    cout << "month = " << date.GetMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test5() // Negative month Test
{
    Date date;

    cout << "Test 5 - Negative month test" << endl;
    date.SetMonth(-1);
    cout << "month = " << date.GetMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test6() // Month greater than 12 Test
{
    Date date;

    cout << "Test 6 - Month greater than 12 test" << endl;
    date.SetMonth(13);
    cout << "month = " << date.GetMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test7() // Month = 0 Test
{
    Date date;

    cout << "Test 7 - Month = 0 test" << endl;
    date.SetMonth(0);
    cout << "month = " << date.GetMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test8() // Day of month setting Test
{
    Date date;

    cout << "Test 8 - Day of month setting test" << endl;
    date.SetDayOfMonth(3);
    cout << "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test9() // Negative day of month Test
{
    Date date;

    cout << "Test 9 - Negative day of month test" << endl;
    date.SetDayOfMonth(-1);
    cout << "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test10() // 31 days in month fail Test
{
    Date date;

    cout << "Test 10 - 31 days in month fail test" << endl;
    date.SetMonth(4);
    date.SetDayOfMonth(31);
    cout << "month = " << date.GetMonth() << endl <<
            "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test11() // 29 days Feb non-leap year Test
{
    Date date;

    cout << "Test 11 - 29 days Feb non-leap year test" << endl;
    date.SetYear(2011);
    date.SetMonth(2);
    date.SetDayOfMonth(29);
    cout << "year = " << date.GetYear() << endl <<
            "month = " << date.GetMonth() << endl <<
            "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test12() // 29 days Feb leap year Test
{
    Date date;

    cout << "Test 12 - 29 days Feb leap year test" << endl;
    date.SetYear(2012);
    date.SetMonth(2);
    date.SetDayOfMonth(29);
    cout << "year = " << date.GetYear() << endl <<
            "month = " << date.GetMonth() << endl <<
            "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test13() // Day of month = 0 Test
{
    Date date;

    cout << "Test 13 - Day of month = 0 test" << endl;
    date.SetDayOfMonth(0);
    cout << "dayOfMonth = " << date.GetDayOfMonth() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test14() // Month prefix Text
{
    Date date;

    cout << "Test 14 - Month prefix test" << endl;
    date.SetMonth(3);
    cout << "monthPrefix = " << date.GetMonthPrefix() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test15()// Number of day Test
{
    Date date;

    cout << "Test 15 - Number of days test" << endl;
    date.SetMonth(9);
    cout << "noDays = " << date.GetNoDays() << endl << endl;
}

//-----------------------------------------------------------------------------
