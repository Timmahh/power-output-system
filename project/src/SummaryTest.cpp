// SummaryTest.cpp

//-----------------------------------------------------------------------------

#include "Summary.h"
#include <iostream>

using namespace std;

//-----------------------------------------------------------------------------

void Test1(); // Constructor test
void Test2(); // Second constructor test
void Test3(); // Negative total power test
void Test4(); // Total output setting test
void Test5(); // Negative maximum output test
void Test6(); // Maximum output setting test
void Test7(); // Time setting test

//-----------------------------------------------------------------------------

int main()
{
    Summary summary;

    cout << "Summary Test Program" << endl << endl;

    Test1();
    Test2();
    Test3();
    Test4();
    Test5();
    Test6();
    Test7();

    cout << endl;
    return 0;
}

//-----------------------------------------------------------------------------

void Test1() // Constructor Test
{
    Summary test01;

    cout << "Test 1 - Constructor test" << endl;
    cout << "total = " << test01.GetTotalOutput() << endl <<
            "max output = " << test01.GetMaxOutput() << endl <<
            "hour = " << test01.GetMaxTime().getHour() << endl <<
            "minute = " << test01.GetMaxTime().getMinute() << endl <<
            "meridiem = " << test01.GetMaxTime().getMeridiem() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test2()  // Second constructor test
{
    cout << "Test 2 - Second constructor test" << endl;
    Time tm = Time();
    Summary test02 = Summary(4.38, 3.1451, tm);
    cout << "total = " << test02.GetTotalOutput() << endl <<
            "max output = " << test02.GetMaxOutput() << endl <<
            "hour = " << test02.GetMaxTime().getHour() << endl <<
            "minute = " << test02.GetMaxTime().getMinute() << endl <<
            "meridiem = " << test02.GetMaxTime().getMeridiem() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test3() // Negative total power test
{
    Summary test01;

    cout << "Test 3 - Negative total power test" << endl;
    test01.SetTotalOutput(-0.01);
    cout << "total power = " << test01.GetTotalOutput() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test4() // Total output setting test
{
    Summary test01;

    cout << "Test 4 - Total output setting test" << endl;
    test01.SetTotalOutput(1.47);
    cout << "total power = " << test01.GetTotalOutput() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test5() // Negative maximum output test
{
    Summary test01;

    cout << "Test 5 - Negative maximum output test" << endl;
    test01.SetMaxOutput(-3.29);
    cout << "max output = " << test01.GetMaxOutput() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test6() // Maximum output setting Test
{
    Summary test01;

    cout << "Test 6 - Maximum output setting test" << endl;
    test01.SetMaxOutput(4.3);
    cout << "max output = " << test01.GetMaxOutput() << endl << endl;
}

void Test7() // Time setting test
{
    Summary test01;

    cout << "Test 7 - Time setting test" << endl;
    test01.SetMaxTime(Time(4, 23, AM));
    cout << "hour = " << test01.GetMaxTime().getHour() << endl <<
            "minute = " << test01.GetMaxTime().getMinute() << endl <<
            "meridiem = " << test01.GetMaxTime().getMeridiem() << endl << endl;
}
