// ReadingTest.cpp

//-----------------------------------------------------------------------------

#include "Reading.h"
#include "Time.h"
#include <iostream>

using namespace std;

//-----------------------------------------------------------------------------

void Test1(); // Constructor test
void Test2(); // Second constructor test
void Test3(); // Time setting test
void Test4(); // Negative power output test
void Test5(); // Power output setting test
void Test6(); // left = right test
void Test7(); // left < right test
void Test8(); // left > right test

//-----------------------------------------------------------------------------

int main()
{
    Reading reading;

    cout << "Reading Test Program" << endl << endl;

    Test1();
    Test2();
    Test3();
    Test4();
    Test5();
    Test6();
    Test7();
    Test8();

    cout << endl;
    return 0;
}

//-----------------------------------------------------------------------------

void Test1() // Constructor Test
{
    Reading test01;

    cout << "Test 1 - Constructor test" << endl;
    cout << "hour = " << test01.GetTime().getHour() << endl <<
            "minute = " << test01.GetTime().getMinute() << endl <<
            "meridiem = " << test01.GetTime().getMeridiem() << endl <<
            "output = " << test01.GetPowerOutput() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test2()  // Second constructor test
{
    cout << "Test 2 - Second constructor test" << endl;
    Time tm = Time(10, 45, AM);
    Reading test02 = Reading(tm, 3.14157);
    cout << "hour = " << test02.GetTime().getHour() << endl <<
            "minute = " << test02.GetTime().getMinute() << endl <<
            "meridiem = " << test02.GetTime().getMeridiem() << endl <<
            "output = " << test02.GetPowerOutput() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test3() // Time setting test
{
    Reading test01;

    cout << "Test 3 - Time setting test" << endl;
    test01.SetTime(Time(2, 45, AM));
    cout << "hour = " << test01.GetTime().getHour() << endl <<
            "minute = " << test01.GetTime().getMinute() << endl <<
            "meridiem = " << test01.GetTime().getMeridiem() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test4() // Negative power output test
{
    Reading test01;

    cout << "Test 4 - Negative power output test" << endl;
    test01.SetPowerOutput(-0.1);
    cout << "output = " << test01.GetPowerOutput() << endl << endl;
}

void Test5() // Power output setting test
{
    Reading test01;

    cout << "Test 5 - Power output setting test" << endl;
    test01.SetPowerOutput(4.282);
    cout << "output = " << test01.GetPowerOutput() << endl << endl;
}

void Test6() // left = right test
{
	cout << "Test 6 - Left = Right test" << endl;
	Reading left;
	Reading right;

	if(left <= right)
	{
		cout << "True" << endl << endl;
	}
	else
	{
		cout << "False" << endl << endl;
	}
}

void Test7() // left < right test
{
	cout << "Test 6 - Left < Right test" << endl;
	Reading left;
	left.SetPowerOutput(1.20);
	Reading right;
	right.SetPowerOutput(3.14);

	if(left <= right)
	{
		cout << "True" << endl << endl;
	}
	else
	{
		cout << "False" << endl << endl;
	}
}

void Test8() // left > right test
{
	cout << "Test 6 - Left > Right test" << endl;
	Reading left;
	left.SetPowerOutput(3.14);
	Reading right;
	right.SetPowerOutput(1.20);

	if(left <= right)
	{
		cout << "True" << endl << endl;
	}
	else
	{
		cout << "False" << endl << endl;
	}
}
