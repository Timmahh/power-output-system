#ifndef TIME_H
#define TIME_H

/**
 * \brief Keeps track of the Meridiem.
 *
 * Keeps track of whether it is ante meridiem (before midday) or post
 * meridiem (after midday).
 */
enum Meridiem
{
	AM, /**< Ante Meridiem (before midday) */
	PM /**< Post Meridiem (after midday) */
};

//------------------------------------------------------------------------------
/**
 * \file Time.h
 * \class Time
 * \brief Time class implementation.
 *
 *      Keeps track of the time in 12 hour time including the current hour,
 *      minute and meridiem.
 *
 * \author Timothy Veletta
 * \date 11/05/2012
 * \version 01 - Timothy Veletta, 11/05/2012
 *          Created the header, implementation and test files as well as the
 *          documentation in the header file.
 * \version 02 - Timothy Veletta, 22/05/12
 *			Added the meridiem type, ampm member variable and changed the
 *			constructors to accomodate for this.
 * \version 03 - Timothy Veletta, 22/05/12
 *			Added a test class and its implementation.
 * \version 04 - Timothy Veletta 23/05/12
 *			Made some changes in relation to the testing including moving the
 *			Meridiem enumeration outside of the class declaration and to the
 *			top of the file.
 * \version 05 - Timothy Veletta, 24/05/12
 *			Added the equality comparison operator.
 */
class Time
{
    public:
         /**
         * \brief Sets the time to midnight.
         *
         * Sets the time to midnight.
         */
        Time();

        /**
        * \brief Sets the time.
        *
        * Sets the time to the input time in 12 hour time.
        *
        * \param hh the hour in 12 hour time
        * \param mm the minute of the hour
        * \param ap the meridiem
        */
        Time(const unsigned hh, const unsigned mm, const Meridiem ap);

        /**
        * \brief Gets the hour.
        *
        * Gets the hour in 24 hour time.
        *
        * \retval the hour
        */
        unsigned getHour() const;

        /**
        * \brief Sets the hour.
        *
        * Sets the hour.
        *
        * \param hh the hour
        * \retval whether or not the operation was successful
        */
        bool setHour(const unsigned hh);

        /**
        * \brief Gets the minute.
        *
        * Gets the minute of the hour.
        *
        * \retval the minute of the hour
        */
        unsigned getMinute() const;

        /**
        * \brief Sets the minute.
        *
        * Sets the minute of the hour.
        *
        * \param mm the minute of the hour
        * \retval whether or not the operation was successful
        */
        bool setMinute(const unsigned mm);

        /**
         * \brief Gets the meridiem.
         *
         * Gets the meridiem of day.
         *
         * \retval the meridiem
         */
        Meridiem getMeridiem() const;

        /**
         * \brief Sets the meridiem.
         *
         * Sets the meridiem to either ante meridiem (before midday) or post
         * meridiem (after midday).
         *
         * \param ap the meridiem
         * \retval whether or not the operation was successful
         */
        bool setMeridiem(const Meridiem ap);

        /**
         * \brief Sets the time in 12 hour time.
         *
         * Sets the time to the input in 12 hour time.
         *
         * \param hh the hour in 12 hour time
         * \param mm the minute of the hour
         * \param ap the meridiem
         * \retval whether or not the operation was successful
         */
        bool setTime(const unsigned hh, const unsigned mm, const Meridiem ap);

		/**
		 * \brief Returns if two Times are equal.
		 *
		 * Compares two Time objects and returns whether or not they are equal.
		 * Two times are said to be equal if the hour, minute and meridiem are
		 * equal.
		 *
		 * \param rhs the object to compare with
		 * \retval whether or not they are equal
		 */
		bool operator ==(const Time &rhs) const;
    private:
        unsigned hour; //!< the hour
        unsigned minute; //!< the minute of the hour
        Meridiem ampm; //!< the meridiem of day
};

//------------------------------------------------------------------------------

#endif // TIME_H
