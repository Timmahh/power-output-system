#ifndef MENU_H
#define MENU_H

//-----------------------------------------------------------------------------

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "Date.h"
#include "PowerRecords.h"

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------
/**
 * \file Menu.h Menu.cpp
 * \class Menu
 * \brief Menu class implementation.
 *
 * Sets up a CLI menu which allows the user to make choices and then displays
 * the output of those choices.
 *
 * \author Timothy Veletta
 * \date 26/05/12
 * \version 01 - Timothy Veletta, 26/05/12
 *          Created the CreateMenu public function and the displayMenu,
 *			getInput, lessThan, greaterThan, equalTo, displayOutput,
 *			displayDate and checkIfDouble helper functions.
 */
class Menu
{
	public:
		/**
		 * \brief Sets up the menu.
		 *
		 * Creates the menu and controls the flow of the program based upon the
		 * choices made by the user.
		 */
		static void CreateMenu();
	protected:
	private:
		static void displayMenu();

		static char getInput();

		static void lessThan();
		static void greaterThan();
		static void equalTo();

		static void displayOutput(vector<Date> output);
		static void displayDate(Date &display);

		static bool checkIfDouble(string val);
};

//-----------------------------------------------------------------------------

#endif // MENU_H
