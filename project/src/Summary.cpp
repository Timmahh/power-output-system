#include "Summary.h"

//-----------------------------------------------------------------------------

Summary::Summary()
{
    totalOutput = 0.0;
    maxOutput = 0.0;
    timeOfMaximum = Time();
}

//-----------------------------------------------------------------------------

Summary::Summary(const double out, const double maxOut, const Time &maxTime)
{
    SetTotalOutput(out);
    SetMaxOutput(maxOut);
    SetMaxTime(maxTime);
}

//-----------------------------------------------------------------------------

double Summary::GetTotalOutput() const
{
    return totalOutput;
}

//-----------------------------------------------------------------------------

bool Summary::SetTotalOutput(const double out)
{
    if(out >= 0.0)
    {
        totalOutput = out;
        return false;
    }
    else
    {
        return false;
    }
}

//-----------------------------------------------------------------------------

double Summary::GetMaxOutput() const
{
    return maxOutput;
}

//-----------------------------------------------------------------------------

bool Summary::SetMaxOutput(const double maxOut)
{
    if(maxOut >= 0.0)
    {
        maxOutput = maxOut;
        return true;
    }
    else
    {
        return false;
    }
}

//-----------------------------------------------------------------------------

Time& Summary::GetMaxTime()
{
    return timeOfMaximum;
}

//-----------------------------------------------------------------------------

void Summary::SetMaxTime(const Time &maxTime)
{
    timeOfMaximum = maxTime;
}

//-----------------------------------------------------------------------------

bool Summary::operator ==(const Summary &rhs) const
{
	return ((totalOutput == rhs.totalOutput) &&
			(maxOutput == rhs.maxOutput) &&
			(timeOfMaximum == rhs.timeOfMaximum));
}

//-----------------------------------------------------------------------------
