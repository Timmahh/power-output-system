#include "FileInput.h"

//-----------------------------------------------------------------------------

string FileInput::directory;
string FileInput::file;
vector<string> FileInput::fileList;

//-----------------------------------------------------------------------------

bool FileInput::setDirectory(const string dir)
{
    directory = dir;

    return createFileList();
}

//-----------------------------------------------------------------------------

bool FileInput::createFileList()
{
    file = "filelist.txt";

    #ifdef _WIN32 // code for windows
    chdir(directory.c_str());

    string command = "dir /B > " + file;

    system(command.c_str());

    #else // code for POSIX
    chdir(directory.c_str());

    string command = "ls > " + file;

    system(command.c_str());
    #endif

    return getFileList();
}

//-----------------------------------------------------------------------------

bool FileInput::getFileList()
{
	ifstream inFile;
	string line;

	inFile.open(file.c_str());

	if(inFile.is_open())
	{
		while(!inFile.eof())
		{
			inFile >> line;
			if(line != "filelist.txt")
			{
			    fileList.push_back(line);
			}
		}

		inFile.close();

		processFiles();

		return true;
	}
	else
	{
		cout << "Error opening file.";
		return false;
	}
}

//-----------------------------------------------------------------------------

bool FileInput::processFiles()
{
	ifstream inFile;
	string line;
	bool read = false;

	for(unsigned i = 0; i < fileList.size(); i++)
	{
		inFile.open(fileList.at(i).c_str());
		read = false;

		if(inFile.is_open())
		{
			while(getline(inFile, line))
			{
				if(read)
				{
					processLine(line);
				}

				if(line.compare("dd/MM/yyyy hh:mm tt;kWh;kW") == 0)
				{
					read = true;
				}
			}

			if(!read)
			{
			    cout << fileList.at(i);
			    cout << " was not of the correct format to read." << endl;
			}

			inFile.close();
		}
		else
		{
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

bool FileInput::processLine(string line)
{
	unsigned dd = 0, mm = 0, yyyy = 0, hh = 0, min = 0;
	char ap = '\0';
	float kw = 0.0;

	sscanf(line.c_str(), "%u/%u/%d %u:%u %c%*c;%*f;%f", &dd, &mm, &yyyy, &hh,
			&min, &ap, &kw);

	Date dt;
	dt.SetDayOfMonth(dd);
	dt.SetMonth(mm);
	dt.SetYear(yyyy);

	Time tm;
	if(ap == 'A')
	{
		tm = Time(hh, min, AM);
	}
	else
	{
		tm = Time(hh, min, PM);
	}

	kw = kw * (1.0/12.0); // gets the output in kWh

	PowerRecords::AddRecord(dt, tm, kw);

	return true;
}

//-----------------------------------------------------------------------------
