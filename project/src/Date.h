#ifndef DATE_H
#define DATE_H

//-----------------------------------------------------------------------------

#include <iostream>
#include <string>

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------

typedef int iArray[12];
const iArray daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//-----------------------------------------------------------------------------
/**
 * \file Date.h
 * \class Date
 * \brief Date class implementation.
 *
 *      Keeps track of the date including the day of the month, month and year.
 *
 * \author Timothy Veletta
 * \date 11/04/2012
 * \version 01 - Timothy Veletta, 11/04/2012
 *          Created header and class file including constructors, get and set
 *          functions and the assignment operator.
 * \version 02 - Timothy Veletta, 11/04/2012
 *          Added Copy method to handle the common code between the copy
 *          constructor and the assignment operator. Also added the
 * \version 03 - Timothy Veletta, 12/04/2012
 *          Added implementation for the default constructor and the output
 *          operator.
 * \version 04 - Timothy Veletta, 12/04/2012
 *          Added implementation for the get and set methods of the year member
 *          variable.
 * \version 05 - Timothy Veletta, 12/04/2012
 *			Added implementation for the get and set methods of the month
 *			member variable and added method stub for the isLeapYear function.
 * \version 06 - Timothy Veletta, 12/04/12
 *			Added implementation for the get and set methods of the
 *			DayOfMonth member variable and also the isLeapYear function.
 * \version 07 - Timothy Veletta, 15/04/12
 *			Removed the 'endl' from the end of the output operator.
 * \version 08 - Timothy Veletta, 21/05/12
 *			Added and tested the isValid function which checks if the input date
 *			is valid.
 * \version 09 - Timothy Veletta, 21/05/12
 * 			Added the less than operator.
 * \version 10 - Timothy Veletta, 28/05/12
 *			CONST ALL THE THINGS!
 */
class Date
{
    public:
        /**
         * \brief The default constructor for the Date class.
         *
         * The default constructor for the Date class sets the Date to the 1st
         * of January 1970.
         */
        Date();

        /**
         * \brief Sets the day, month and year of the Date class.
         *
         * This constructor for the Date class allows the day of the month,
         * month and year to be set.
         *
         * \param dd The day of the month
         * \param mm The month
         * \param yyyy The year
         */
        Date(const int dd, const int mm, const int yyyy);

        /**
         * \brief Access dayOfMonth
         *
         * Returns the current value of the day in the given month.
         *
         * \retval The current value of dayOfMonth
         */
        int GetDayOfMonth() const;

        /**
         * \brief Set dayOfMonth
         *
         * Sets the value of the day in the given month to the given value.
         *
         * \param val New value to set
         */
        bool SetDayOfMonth(const int val);

        /**
         * \brief Access month
         *
         * Returns the current value of the month.
         *
         * \retval The current value of month
         */
        int GetMonth() const;

        /**
         * \brief Set month
         *
         * Sets the value of the month to the given value.
         *
         * \param val New value to set
         */
        bool SetMonth(const int val);

        /**
         * \brief Access year
         *
         * Returns the current value of the year.
         *
         * \retval The current value of year
         */
        int GetYear() const;

        /**
         * \brief Set year
         *
         * Sets the value of the year to the given value.
         *
         * \param val New value to set
         */
        bool SetYear(const int val);

        /**
         * \brief Returns the number of days in the current month.
         *
         * Returns the number of days in the current month which is specified
         * by the Month member variable.
         *
         * \retval The number of days in the month
         */
        unsigned GetNoDays() const;

        /**
         * \brief Returns the month prefix.
         *
         * Returns a 3 letter prefix for the current month which is specified
         * by the Month member variable.
         *
         * \retval The month prefix
         */
		string GetMonthPrefix() const;

		/**
		 * \brief Less than operator for Date.
		 *
		 * The less than operator for the Date returns whether or not the
		 * current instance is less than the input instance of Date.
		 *
		 * \param rhs the Date to compare with
		 * \retval whether or not the current instance is less than the input
		 */
        bool operator <(const Date &rhs) const;
    protected:
    private:
        int dayOfMonth; //!< Member variable "dayOfMonth"
        int month; //!< Member variable "month"
        int year; //!< Member variable "year"

        bool isLeapYear(const int val) const;
        bool isValidDate(const int dd, const int mm, const int yyyy) const;
};

//-----------------------------------------------------------------------------

#endif // DATE_H
