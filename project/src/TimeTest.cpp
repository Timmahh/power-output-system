// DateTest.cpp

//------------------------------------------------------------------------------

#include "Time.h"

#include <iostream>

using namespace std;

//------------------------------------------------------------------------------

void Test1(); // Constructor test
void Test2(); // Second constructor test
void Test3(); // Hour less than 1 test
void Test4(); // Hour greater than 12 test
void Test5(); // Hour setting test
void Test6(); // Negative minute test
void Test7(); // Minute greater than 59 test
void Test8(); // Setting minute test
//void Test9(); // Invalid meridiem test
void Test10(); // Setting meridiem test

//-----------------------------------------------------------------------------

int main()
{
    cout << "Time Test Program" << endl << endl;

    Test1();
    Test2();
    Test3();
    Test4();
    Test5();
    Test6();
    Test7();
    Test8();
    //Test9();
    Test10();

    cout << endl;
    return 0;
}

//-----------------------------------------------------------------------------

void Test1() // Constructor test
{
    Time test01;

    cout << "Test 1 - Constructor test" << endl;
    cout << "hour = " << test01.getHour() << endl <<
            "minute = " << test01.getMinute() << endl <<
            "meridiem = " << test01.getMeridiem() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test2()  // Second constructor test
{
    Time test02;

    cout << "Test 2 - Second constructor test" << endl;
    test02 = Time(5, 15, PM);
    cout << "hour = " << test02.getHour() << endl <<
            "minute = " << test02.getMinute() << endl <<
            "meridiem = " << test02.getMeridiem() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test3() // Hour less than 1 test
{
    Time test01;

    cout << "Test 3 - Hour less than 1 test" << endl;
    test01.setHour(0);
    cout << "hour = " << test01.getHour() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test4() // Hour greater than 12 test
{
    Time test01;

    cout << "Test 4 - Hour greater than 12 test" << endl;
    test01.setHour(13);
    cout << "hour = " << test01.getHour() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test5() // Hour setting test
{
    Time test01;

    cout << "Test 5 - Hour setting test" << endl;
    test01.setHour(3);
    cout << "hour = " << test01.getHour() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test6() // Negative minute test
{
    Time test01;

    cout << "Test 6 - Negative minute test" << endl;
    test01.setMinute(-1);
    cout << "minute = " << test01.getMinute() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test7() // Minute greater than 59 test
{
    Time test01;

    cout << "Test 7 - Minute greater than 59 test" << endl;
    test01.setMinute(60);
    cout << "minute = " << test01.getMinute() << endl << endl;
}

//-----------------------------------------------------------------------------

void Test8() // Setting minute test
{
    Time test01;

    cout << "Test 8 - Setting minute test" << endl;
    test01.setMinute(36);
    cout << "minute = " << test01.getMinute() << endl << endl;
}

//-----------------------------------------------------------------------------

/*void Test9() // Invalid meridiem test
{
    Time test01;

    cout << "Test 9 - Invalid meridiem test" << endl;
    test01.setMeridiem(31);
    cout << "meridiem = " << test01.getMeridiem() << endl << endl;
}*/

//-----------------------------------------------------------------------------

void Test10() // Setting meridiem test
{
    Time test01;

    cout << "Test 10 - Setting meridiem test" << endl;
    test01.setMeridiem(PM);
    cout << "meridiem = " << test01.getMeridiem() << endl << endl;
}

//-----------------------------------------------------------------------------
