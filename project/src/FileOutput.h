#ifndef FILEOUTPUT_H
#define FILEOUTPUT_H

//-----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <fstream>
#include "PowerRecords.h"

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------
/**
 * \file FileOutput.h FileOutput.cpp
 * \class FileOutput
 * \brief File Output class implementation.
 *
 * Outputs the information about a particular month to a file named based upon
 * the month and year of the records. Output is of the form:
 * Date;output in kWh for the day;maximum output for the day;time of maximum
 * output for the day
 *
 * \author Timothy Veletta
 * \date 26/05/12
 * \version 01 - Timothy Veletta, 26/05/12
 *          Created the outputMonth function.
 */
class FileOutput
{
	public:
		/**
		 * \brief Outputs the information about a particular month.
		 *
		 * Outputs the record information about a particular month to a file
		 * named based upon the records month and year.
		 *
		 * \param monthAndYear the month and year of the record
		 * \retval whether or not the output file could be opened for output
		 */
		static bool outputMonth(const MonthYear monthAndYear);
};

//-----------------------------------------------------------------------------

#endif // FILEOUTPUT_H
