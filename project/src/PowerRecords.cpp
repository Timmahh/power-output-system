#include "PowerRecords.h"

//-----------------------------------------------------------------------------

RecordsAtDate PowerRecords::records;
SummaryAtDate PowerRecords::summaries;
double PowerRecords::currentTotal;
double PowerRecords::currentMaxOutput;
Time PowerRecords::currentMaxTime;

//-----------------------------------------------------------------------------

PowerRecords::PowerRecords()
{
    currentTotal = 0.0;
    currentMaxOutput = 0.0;
    currentMaxTime = Time();
}

//-----------------------------------------------------------------------------

bool PowerRecords::AddRecord(const Date &dateOfRecord,
							const Time &timeOfRecord,
							const double powerOutput)
{
	Reading record = Reading(timeOfRecord, powerOutput);

	return records[dateOfRecord].insert(record);
}

//-----------------------------------------------------------------------------

void PowerRecords::CreateSummaries()
{
	for(RecCItr itr = records.begin(); itr != records.end(); itr++)
	{
		currentTotal = 0.0;
		currentMaxOutput = 0.0;
		currentMaxTime = Time();

		(itr->second).inOrderTraversal(total);
		(itr->second).inOrderTraversal(maximum);

		summaries[itr->first] = Summary(currentTotal, currentMaxOutput,
										currentMaxTime);
	}
}

//-----------------------------------------------------------------------------

monthYearVector PowerRecords::GetTimePeriods()
{
	MonthYear temp;
	double currentMonth;
	double currentYear;

	monthYearVector timePeriods;
	monthItr timePeriodsItr;

	for(SumCItr itr = summaries.begin(); itr != summaries.end(); itr++)
	{
		temp.month = (itr->first).GetMonth();
		temp.year = (itr->first).GetYear();

		if(timePeriods.size() == 0)
		{
			timePeriods.push_back(temp);
		}
		else
		{
			bool exists = false;

			for(unsigned i = 0; i < timePeriods.size(); i++)
			{
				currentMonth = timePeriods.at(i).month;
				currentYear = timePeriods.at(i).year;

				if(currentMonth == temp.month && currentYear == temp.year)
				{
					exists = true;
				}
			}

			if(!exists)
			{
				timePeriods.push_back(temp);
			}
		}
	}

	return timePeriods;
}

//-----------------------------------------------------------------------------

Summary& PowerRecords::GetSummary(const Date &summaryDate)
{
	return summaries[summaryDate];
}

//-----------------------------------------------------------------------------

vector<Date> PowerRecords::GetLessThan(double outputValue)
{
	vector<Date> output;

	for(SumCItr itr = summaries.begin(); itr != summaries.end(); itr++)
	{
		if((itr->second).GetTotalOutput() < outputValue)
		{
			output.push_back(itr->first);
		}
	}

	return output;
}

//-----------------------------------------------------------------------------

vector<Date> PowerRecords::GetGreaterThan(double outputValue)
{
	vector<Date> output;

	for(SumCItr itr = summaries.begin(); itr != summaries.end(); itr++)
	{
		if((itr->second).GetTotalOutput() > outputValue)
		{
			output.push_back(itr->first);
		}
	}

	return output;
}

//-----------------------------------------------------------------------------

vector<Date> PowerRecords::GetEqualTo(double outputValue)
{
	vector<Date> output;

	for(SumCItr itr = summaries.begin(); itr != summaries.end(); itr++)
	{
		if(((itr->second).GetTotalOutput() < outputValue + 0.001) &&
			(itr->second).GetTotalOutput() > outputValue - 0.001)
		{
			output.push_back(itr->first);
		}
	}

	return output;
}

//-----------------------------------------------------------------------------

void PowerRecords::maximum(Reading &val)
{
	if(val.GetPowerOutput() > currentMaxOutput)
	{
		currentMaxOutput = val.GetPowerOutput();
		currentMaxTime = val.GetTime();
	}
}

//-----------------------------------------------------------------------------

void PowerRecords::total(Reading &val)
{
	currentTotal += val.GetPowerOutput();
}

//-----------------------------------------------------------------------------
