#ifndef READING_H
#define READING_H

//-----------------------------------------------------------------------------

#include "Time.h"

//-----------------------------------------------------------------------------

/**
 * \file Reading.h Reading.cpp
 * \class Reading
 * \brief Reading class implementation.
 *
 * Holds information regarding the Time of a reading and the power output at
 * that time.
 *
 * \author Timothy Veletta
 * \date 26/05/12
 * \version 01 - Timothy Veletta, 26/05/12
 *          Created the default constructor, second constructor and the get and
 *          set functions for the member variables, timeOfDay and powerOutput.
 * \version 02 - Timothy Veletta, 27/05/12
 *			Added the assignment operator.
 */
class Reading
{
    public:
        /**
         * \brief The default constructor for the reading class.
         *
         * The default constructor for the Reading class calls the default
         * constructor for the time and sets the output to 0.
         */
        Reading();

        /**
         * \brief Sets the time and output for the reading.
         *
         * This constructor sets the time of the day and the power output for
         * the given reading.
         *
         * \param tm the time of the reading
         * \param out the output at that time
         */
        Reading(const Time &tm, const double out);

        /**
         * \brief Gets the time of the reading.
         *
         * Gets the time of day of the reading.
         *
         * \retval the time of the day
         */
        Time GetTime() const;

        /**
         * \brief Sets the time of the reading.
         *
         * Sets the time of the day of the reading to the input value.
         *
         * \param tm the time of the day
         */
        void SetTime(const Time &tm);

        /**
         * \brief Gets the power output.
         *
         * Gets the power output for the reading.
         *
         * \retval the power output
         */
        double GetPowerOutput() const;

        /**
         * \brief Sets the power output.
         *
         * Sets the power output of the reading to the input value. This value
         * cannot be less than 0.
         *
         * \param out the power output
         * \retval whether or not the operation was successful
         */
        bool SetPowerOutput(const double out);

		/**
		 * \brief The less than or equal to operator.
		 *
		 * The less than or equal to operator for the Reading class. Returns
		 * true if the value of the power output on the left is less than
		 * or equal to the value of the power output on the right hand side.
		 *
		 * \param rhs the other Reading object to compare
		 * \retval whether or not the left object is less than or equal to the
		 * right object
		 */
		bool operator <=(const Reading &rhs) const;

		/**
		 * \brief The assignment operator.
		 *
		 * The assignment operator for the Reading class.
		 *
		 * \param other object to assign from
		 * \retval object to assign to
		 */
		Reading& operator =(const Reading &other);
    protected:
    private:
        Time timeOfDay; //!< the time of the day
        double powerOutput; //!< the power output
};

//-----------------------------------------------------------------------------

#endif // READING_H
